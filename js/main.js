$(document).ready(function() {
    var swiper = new Swiper('.home-slider', {
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        autoplay: {
            delay: 5000,
        },
        effect: 'fade',
        speed: 900,
    });
    var swiper = new Swiper('.home-featured-slider', {
        slidesPerView: 'auto',
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });
    var swiper = new Swiper('.sponsors-container .swiper-container', {
        slidesPerView: 'auto',
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        autoplay: {
            delay: 3000,
        },
        speed: 900,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });
    $('.menu-button').click(function(e) {
        e.preventDefault();
        e.stopPropagation();
        $('body').toggleClass('menu-active')
    })
    $('.animation').each(function() {
        var x = $(this).offset().top;
        var y = $(window).height();
        var z = $(window).scrollTop();

        if (x < (z + (y))) {
            $(this).addClass("animation-ready");
        } else {
            $(this).removeClass("");
        }
    });
    $(window).scroll(function() {
        $('.animation').each(function() {
            var x = $(this).offset().top;
            var y = $(window).height();
            var z = $(window).scrollTop();

            if (x < (z + (y))) {
                $(this).addClass("animation-ready");
            } else {
                $(this).removeClass("");
            }
        });
    });
    $('.child-menu-item').click(function(){
        $('.child-menu-item').removeClass('active')
        $(this).addClass('active');
        var idT = '#' + $(this).attr('data-target');
        $(idT).click();
    });
    $('.remove-options').click(function(){
        $('.child-menu-item').removeClass('active')
    })
});